
const express = require('express');
const app = express();
const port = 4000;

app.use(express.json() );

// Mock Data
let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "weDontTalkAboutMe"
	},
	{
		username: "LuisaMadrigal",
		email: "strongSis@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	}
];

//  Get All users and items respectively
app.get ('/users/items', (req, res) =>{
	res.send([users,items])
})




// Get single item using index

app.get('/items/getSingleItems/:index', (req, res) => {
	let index = parseInt(req.params.index);
	res.send(items[index])
});


// Deactivating items

app.get('/items/archive/:index', (req, res) => {
	let index = parseInt(req.params.index)
	items[index].isActive = false;
	res.send(items[index])
})



// Activating items

app.get('/items/activate/:index', (req, res) => {
	console.log();

	let index = parseInt(req.params.index);
	items[index].isActive = true;
	res.send(items[index])
})


// Automatic reassignment (activate/deactivate)

app.get('/items/auto/:index', (req, res) => {
	console.log(items);
	console.log(req.params);

	// converts the req params from a string to integer then stored into index variable
	let index = parseInt(req.params.index);

	// switch boolean into its inverse value
	items[index].isActive = !items[index].isActive;
	res.send(items[index]);
	
})




app.listen(port, () => console.log(`Server is running at port ${port}`))
